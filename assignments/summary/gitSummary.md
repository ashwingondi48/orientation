### **What is Git ?**

Git is a version control (VCS) system for tracking changes to projects. Version control systems are also called revision control systems or source code management (SCM) systems it is used for tracking changes in source code during software development. It is designed for coordinating work among programmers, but it can be used to track changes in any set of files. Its goals include speed, data integrity, and support for distributed, nonlinear workflows.

![](https://www.w3docs.com/uploads/media/default/0001/03/48bf04268538a4f24e79ed04eac23953fafed3cd.png)

### **Getting started with Git**

**Installing Git**

- For Linux: sudo apt-get install git

- For Windows: Download the git installer and run it

**Firstly share name and email address**

`git config --global user.name "Your Name Comes Here"`


`git config --global user.email your_email@yourdomain.example.com`

### **Starting a new project on git**

In order to start a new project on git we use the linux command mkdir(make directory) to create a new directory to store data of our project using the command given below

`mkdir project_name`

In order to access a project that already exists we use the cd (change directory) command as shown below:

`cd project_name`

In order to delete a project file we use the linux command rmdir (remove directory) to delete the directory permanently

`rmdir project_name`

### **Definition of Commands in GIT**


1. git status: check status and see what has changed

2. git add: add a changed file or a new file to be committed

3. git diff: see the changes between the current version of a file and the version of the file t recently committed

4. git commit: commit changes to the history

5. git log: show the history for a project

6. git revert: undo a change introduced by a specific commit

7. git checkout: switch branches or move within a branch

8. git clone: clone a remote repository

9. git pull: pull changes from a remote repoository

10. git push: push changes to remote repository

11. git checkout -b (branch name): create new branch

### **Block Diagram of GIT workflow**

![Here is an image](https://nexuslinkservices.com/wp-content/uploads/2020/01/Git.jpg)

### **Understanding the workflow**

![](https://www.git-tower.com/learn/git/ebook/en/command-line/remote-repositories/introduction/basic-remote-workflow.png)

### **Why GIT?**

If you are working on a group assignment with some colleagues, and everyone wants their ideas to be considered .Now, to accomplish everyone's needs, There are 2 ways to combine these ideas together without compromising on anybody's ideas:
- Person 1 writes his/her/their ideas on a single sheet of paper and passes it on to Person 2, who writes his/her/their ideas on the paper and passes it on to Person 3, etc.
- Everyone writes their ideas down on separate pieces of paper, and then consolidates them into a single sheet afterward.
