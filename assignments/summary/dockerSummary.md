### **What is a  Docker?**

An open-source project that automates the deployment of software applications inside containers by providing an additional layer of abstraction and automation of OS-level virtualization on Linux.

**Block Diagram:**

![](https://www.whizlabs.com/blog/wp-content/uploads/2019/08/Docker_Architecture.png)

### **Why should we use Docker?**

_For developers, there arises a situation where they need to work on different projects. And for each different project they might need a specific operating system, or a specific version of operating system. This becomes a tedious job to do. So problem can be solved using virtual machines, but this requires exact enviornment to run an application. Here we use Docker. The developer can work on the application and save it as docker image and create a docker container. This container has everything which is required to run an application alongwith the enviornment.This docker is then stored to repository and then can be used by other developers. So docker is used by many developers._

### **Some Docker Terminologies to remember**

- Docker
- Docker Image
- Container
- Docker Hub


### **Docker Installation**

- **Installing Community Engine steps for ubuntu:**


        $ sudo apt-get update
        $ sudo apt-get install \
            apt-transport-https \
            ca-certificates \
            curl \
            gnupg-agent \
            software-properties-common
        $ curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
        $ sudo apt-key fingerprint 0EBFCD88
        $ sudo add-apt-repository \
            "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
            $(lsb_release -cs) \
            stable nightly test"
        $ sudo apt-get update
        $ sudo apt-get install docker-ce docker-ce-cli containerd.io

        // Check if docker is successfully installed in your system
        $ sudo docker run hello-world

- For more assistance on Installation click [here](https://docs.docker.com/engine/install/ubuntu/)

### **Docker Basic commands**

* `$ docker run hello world`- Docker hello world  
* `$ docker images` - Check number of docker images on your system  
* `$ docker search <image>` – Search an image in the Docker Hub   
* `$ docker run` – Runs a command in a new container.  
* `$ docker start` – Starts one or more stopped containers  
* `$ docker stop` – Stops one or more running containers 
